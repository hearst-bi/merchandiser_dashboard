
#%%
from google.cloud import bigquery
import pandas as pd
import numpy as np
from datetime import datetime, date
from pytz import timezone

import pydata_google_auth
credentials = pydata_google_auth.get_user_credentials(
    ['https://www.googleapis.com/auth/bigquery','https://www.googleapis.com/auth/drive','https://www.googleapis.com/auth/spreadsheets','https://www.googleapis.com/auth/cloud-platform',],)
client = bigquery.Client(project='newspapers-142716', credentials=credentials)

import gspread
gc = gspread.authorize(credentials)

query_params = [
    bigquery.ScalarQueryParameter('Site', 'STRING', ''),
    ]

# Set up the sitemap to rename the publications to match across all datasets
site_map = {
    'SFGate':'sfgate',
    'sfgate':'sfgate',
    'MySA.com':'mysa',
    'mysa':'mysa',
    'SeattlePI':'seattlepi',
    'Chron.com':'chron',
    'chron':'chron',
    'All Connecticut':'CT',
    'CT Global':'CT',
    'CT':'CT',
    'Laredo MT' : 'laredo',
    'laredo':'laredo'}

# SQLs for pulling pageviews and commerce data with ipid set to 'article merchandiser', 
am_ipid_commerce_sql = """
    # https://hearst.looker.com/explore/commerce/skimlinks_comm?toggle=fil&qid=doZkwrUfuv1BvhiF3f3wzN
    
    SELECT
        (FORMAT_TIMESTAMP('%F', TIMESTAMP_TRUNC(skimlinks_comm.click_date , WEEK(MONDAY)))) AS skimlinks_comm_click_week,
        hnp_account_mapping.UnifiedPublicationName  AS hnp_account_mapping_unified_publication_name,
        REGEXP_EXTRACT(LOWER(page_url),r'\?ipid=([^[&#]*)')  AS skimlinks_comm_ipid,
        CASE
        WHEN
          skimlinks_comm.normalized_page_url LIKE "%chron.com/shopping/cs/%" THEN CAST(REGEXP_EXTRACT(skimlinks_comm.normalized_page_url, r'\d+$') AS INT64)
        WHEN
          skimlinks_comm.normalized_page_url like "%.php%" THEN CAST(REGEXP_EXTRACT(skimlinks_comm.normalized_page_url, r'\-(\d*).php') AS INT64)
        ELSE
          NULL
      END   AS skimlinks_comm_article_content_id,
        COALESCE(SUM(CASE WHEN cancelled IS NOT true AND items_count != 0 THEN 1 ELSE 0 END), 0) AS skimlinks_comm_sales,
        COALESCE(SUM(skimlinks_comm.publisher_commission_amount ), 0) AS skimlinks_comm_publisher_commission_amount
    FROM `newspapers-142716.skimlinks.comm`
        AS skimlinks_comm
    INNER JOIN common.hnp_account_mapping  AS hnp_account_mapping ON hnp_account_mapping.SkimlinksPublisherDomainId LIKE CONCAT('%', (CAST(skimlinks_comm.publisher_domain_id AS STRING)), '%')
    WHERE (REGEXP_EXTRACT(LOWER(page_url),r'\?ipid=([^[&#]*)') ) = 'article-merchandiser' AND ((( CAST(skimlinks_comm.transaction_datetime AS TIMESTAMP) ) >= ((TIMESTAMP_ADD(TIMESTAMP_TRUNC(TIMESTAMP_TRUNC(CURRENT_TIMESTAMP(), DAY), WEEK(MONDAY)), INTERVAL (-8 * 7) DAY))) AND ( CAST(skimlinks_comm.transaction_datetime AS TIMESTAMP) ) < ((TIMESTAMP_ADD(TIMESTAMP_ADD(TIMESTAMP_TRUNC(TIMESTAMP_TRUNC(CURRENT_TIMESTAMP(), DAY), WEEK(MONDAY)), INTERVAL (-8 * 7) DAY), INTERVAL (8 * 7) DAY)))))
    GROUP BY
        1,
        2,3,4
"""

am_ipid_pvs = """
    # https://hearst.looker.com/explore/newspapers/ga_web_sessions?toggle=fil&qid=1yngeRSzbL4wKkfjdyW9sh
    WITH ga_web_sessions AS (SELECT _PARTITIONTIME AS partitiontime, *, (SELECT MAX(PagePath) FROM UNNEST(Hits) WHERE IsEntrance) AS entrance_page_url, (SELECT ANY_VALUE(ContentType) FROM UNNEST(Hits)) AS entrance_content_type
    FROM ga.ga_web_sessions )
    ,  hnp_web_account_mapping AS (SELECT * FROM common.hnp_account_mapping WHERE AccountType = "Website"
        )
    SELECT
        (FORMAT_TIMESTAMP('%F', TIMESTAMP_TRUNC(ga_web_sessions.partitiontime, WEEK(SUNDAY)))) AS ga_web_sessions_visited_date_week,
        hnp_web_account_mapping.UnifiedPublicationName  AS hnp_web_account_mapping_unified_publication_name,
        ga_web_sessions_hits.ContentId  AS ga_web_sessions_hits_content_id,
        COUNT(CASE WHEN (ga_web_sessions_hits.Type = 'PAGE') THEN CONCAT(( CONCAT(CAST(ga_web_sessions.ViewId AS STRING), "-", ga_web_sessions.FullVisitorId, '|', COALESCE(CAST(ga_web_sessions.VisitNumber AS STRING),''), "-", CAST(ga_web_sessions.VisitStartTime AS STRING))  ),'|',CAST( ga_web_sessions_hits.HitNumber   AS STRING))  ELSE NULL END) AS ga_web_sessions_hits_page_views,
        COUNT(DISTINCT ga_web_sessions_hits.ContentId ) AS ga_web_sessions_hits_content_id_count
    FROM ga_web_sessions
    INNER JOIN hnp_web_account_mapping ON ga_web_sessions.ViewId = hnp_web_account_mapping.GA_ViewId

    CROSS JOIN UNNEST(ga_web_sessions.Hits) as ga_web_sessions_hits
    WHERE ((( ga_web_sessions.partitiontime ) >= ((TIMESTAMP_ADD(TIMESTAMP_TRUNC(TIMESTAMP_TRUNC(CURRENT_TIMESTAMP(), DAY), WEEK(SUNDAY)), INTERVAL (-8 * 7) DAY))) 
    AND ( ga_web_sessions.partitiontime ) < ((TIMESTAMP_ADD(TIMESTAMP_ADD(TIMESTAMP_TRUNC(TIMESTAMP_TRUNC(CURRENT_TIMESTAMP(), DAY), WEEK(SUNDAY)), INTERVAL (-8 * 7) DAY), INTERVAL (8 * 7) DAY))))) 
    AND (ga_web_sessions_hits.Type = 'PAGE'
        AND ((lower(ga_web_sessions.VisitReferrerDomain)) NOT LIKE '%olive%' AND LOWER((lower(ga_web_sessions.VisitReferrerDomain))) NOT LIKE '%eedition%' OR (lower(ga_web_sessions.VisitReferrerDomain)) IS NULL)
        AND (ga_web_sessions.UtmCampaign NOT LIKE 'arb_%' OR ga_web_sessions.UtmCampaign IS NULL)
        AND (ga_web_sessions.entrance_page_url NOT LIKE '%bot-traffic.icu%')) AND ((UPPER(( hnp_web_account_mapping.UnifiedPublicationName  )) = UPPER('SFGate') OR UPPER(( hnp_web_account_mapping.UnifiedPublicationName  )) = UPPER('MySA.com') OR UPPER(( hnp_web_account_mapping.UnifiedPublicationName  )) = UPPER('Chron.com') OR UPPER(( hnp_web_account_mapping.UnifiedPublicationName  )) = UPPER('SeattlePI'))) AND (CASE WHEN (LOWER(replace(ga_web_sessions_hits.QueryString,"%20",""))) like '%return%' AND (LOWER(replace(ga_web_sessions_hits.QueryString,"%20",""))) not like '?return%' AND (LOWER(replace(ga_web_sessions_hits.QueryString,"%20",""))) not like '%&return%' AND (LOWER(replace(ga_web_sessions_hits.QueryString,"%20",""))) like '%ipid%' THEN REGEXP_EXTRACT(REGEXP_EXTRACT(CONCAT("&", (LOWER(replace(ga_web_sessions_hits.QueryString,"%20",""))), "&"), r'[?|&]ipid=([\w-./_]*)'), r'(.*)return')
        WHEN REGEXP_EXTRACT(CONCAT("&", (LOWER(replace(ga_web_sessions_hits.QueryString,"%20",""))), "&"), r'[?|&]ipid=([\w-.]*)') like '%return%' THEN REGEXP_EXTRACT(REGEXP_EXTRACT(CONCAT("&", (LOWER(replace(ga_web_sessions_hits.QueryString,"%20",""))), "&"), r'[?|&]ipid=([\w-./_]*)'), r'(.*)return')
        ELSE IF(NOT REGEXP_CONTAINS(ga_web_sessions_hits.pagepath, "legacy.com"), REGEXP_EXTRACT(CONCAT("&", (LOWER(replace(ga_web_sessions_hits.QueryString,"%20",""))), "&"), r'[?|&]ipid=([\w-./_]*)'), NULL)
        END
        ) LIKE '%article-merchandiser%'
    GROUP BY
        1,
        2,3
"""

am_ipid_pvs_detail_sql = """
    # https://hearst.looker.com/explore/newspapers/ga_web_sessions?qid=jiB29IhBMQKnjRRHAJRTFf
    
    WITH ga_web_sessions AS (SELECT _PARTITIONTIME AS partitiontime, *, (SELECT MAX(PagePath) FROM UNNEST(Hits) WHERE IsEntrance) AS entrance_page_url, (SELECT ANY_VALUE(ContentType) FROM UNNEST(Hits)) AS entrance_content_type
    FROM ga.ga_web_sessions )
    ,  hnp_web_account_mapping AS (SELECT * FROM common.hnp_account_mapping WHERE AccountType = "Website"
        )
    SELECT
        (FORMAT_TIMESTAMP('%F', TIMESTAMP_TRUNC(ga_web_sessions.partitiontime, WEEK(SUNDAY)))) AS ga_web_sessions_visited_date_week,
        hnp_web_account_mapping.UnifiedPublicationName  AS hnp_web_account_mapping_unified_publication_name,
        ga_web_sessions_hits.ContentId  AS ga_web_sessions_hits_content_id,
        ga_web_sessions_hits.truereferrer  AS ga_web_sessions_hits_true_referrer,
        COUNT(CASE WHEN (ga_web_sessions_hits.Type = 'PAGE') THEN CONCAT(( CONCAT(CAST(ga_web_sessions.ViewId AS STRING), "-", ga_web_sessions.FullVisitorId, '|', COALESCE(CAST(ga_web_sessions.VisitNumber AS STRING),''), "-", CAST(ga_web_sessions.VisitStartTime AS STRING))  ),'|',CAST( ga_web_sessions_hits.HitNumber   AS STRING))  ELSE NULL END) AS ga_web_sessions_hits_page_views,
        COUNT(DISTINCT ga_web_sessions_hits.ContentId ) AS ga_web_sessions_hits_content_id_count
    FROM ga_web_sessions
    INNER JOIN hnp_web_account_mapping ON ga_web_sessions.ViewId = hnp_web_account_mapping.GA_ViewId

    CROSS JOIN UNNEST(ga_web_sessions.Hits) as ga_web_sessions_hits
    WHERE ((( ga_web_sessions.partitiontime ) >= ((TIMESTAMP_ADD(TIMESTAMP_TRUNC(TIMESTAMP_TRUNC(CURRENT_TIMESTAMP(), DAY), WEEK(SUNDAY)), INTERVAL (-8 * 7) DAY))) AND ( ga_web_sessions.partitiontime ) < ((TIMESTAMP_ADD(TIMESTAMP_ADD(TIMESTAMP_TRUNC(TIMESTAMP_TRUNC(CURRENT_TIMESTAMP(), DAY), WEEK(SUNDAY)), INTERVAL (-8 * 7) DAY), INTERVAL (8 * 7) DAY))))) AND (ga_web_sessions_hits.Type = 'PAGE'
        AND ((lower(ga_web_sessions.VisitReferrerDomain)) NOT LIKE '%olive%' AND LOWER((lower(ga_web_sessions.VisitReferrerDomain))) NOT LIKE '%eedition%' OR (lower(ga_web_sessions.VisitReferrerDomain)) IS NULL)
        AND (ga_web_sessions.UtmCampaign NOT LIKE 'arb_%' OR ga_web_sessions.UtmCampaign IS NULL)
        AND (ga_web_sessions.entrance_page_url NOT LIKE '%bot-traffic.icu%')) AND ((UPPER(( hnp_web_account_mapping.UnifiedPublicationName  )) = UPPER('SFGate') OR UPPER(( hnp_web_account_mapping.UnifiedPublicationName  )) = UPPER('MySA.com') OR UPPER(( hnp_web_account_mapping.UnifiedPublicationName  )) = UPPER('Chron.com') OR UPPER(( hnp_web_account_mapping.UnifiedPublicationName  )) = UPPER('SeattlePI'))) AND (CASE WHEN (LOWER(replace(ga_web_sessions_hits.QueryString,"%20",""))) like '%return%' AND (LOWER(replace(ga_web_sessions_hits.QueryString,"%20",""))) not like '?return%' AND (LOWER(replace(ga_web_sessions_hits.QueryString,"%20",""))) not like '%&return%' AND (LOWER(replace(ga_web_sessions_hits.QueryString,"%20",""))) like '%ipid%' THEN REGEXP_EXTRACT(REGEXP_EXTRACT(CONCAT("&", (LOWER(replace(ga_web_sessions_hits.QueryString,"%20",""))), "&"), r'[?|&]ipid=([\w-./_]*)'), r'(.*)return')
        WHEN REGEXP_EXTRACT(CONCAT("&", (LOWER(replace(ga_web_sessions_hits.QueryString,"%20",""))), "&"), r'[?|&]ipid=([\w-.]*)') like '%return%' THEN REGEXP_EXTRACT(REGEXP_EXTRACT(CONCAT("&", (LOWER(replace(ga_web_sessions_hits.QueryString,"%20",""))), "&"), r'[?|&]ipid=([\w-./_]*)'), r'(.*)return')
        ELSE IF(NOT REGEXP_CONTAINS(ga_web_sessions_hits.pagepath, "legacy.com"), REGEXP_EXTRACT(CONCAT("&", (LOWER(replace(ga_web_sessions_hits.QueryString,"%20",""))), "&"), r'[?|&]ipid=([\w-./_]*)'), NULL)
        END
        ) LIKE '%article-merchandiser%' AND ((ga_web_sessions_hits.truereferrer ) <> '(not set)' OR (ga_web_sessions_hits.truereferrer ) IS NULL)
    GROUP BY
        1,
        2,
        3,
        4
"""

job_config = bigquery.QueryJobConfig()
job_config.query_parameters = query_params
am_ipid_commerce_df = client.query(
    am_ipid_commerce_sql, job_config=job_config).to_dataframe()

am_ipid_pvs_df = client.query(
    am_ipid_pvs, job_config=job_config).to_dataframe()

am_ipid_pvs_detail_df = client.query(
    am_ipid_pvs_detail_sql, job_config=job_config).to_dataframe()

# Column renames and datatype reformats to match datasets
am_ipid_commerce_df.columns = ['click_week','publication','ipid','related_content_id','sales','commission']
am_ipid_commerce_df['publication'] = am_ipid_commerce_df['publication'].map(site_map)
am_ipid_commerce_df['click_week'] = pd.to_datetime(am_ipid_commerce_df['click_week'])
am_ipid_commerce_df['visited_week'] = am_ipid_commerce_df['click_week'] - np.timedelta64(1,'D')
am_ipid_commerce_df['link_type'] = "Related"
am_ipid_commerce_df['related_content_id'] = am_ipid_commerce_df['related_content_id'].astype(str)


am_ipid_pvs_df.columns = ['visited_week','publication','related_content_id','page_views','articles']
am_ipid_pvs_df['publication'] = am_ipid_pvs_df['publication'].map(site_map)
am_ipid_pvs_detail_df.columns = ['visited_week','publication','related_content_id','merchandized_url','page_views','articles']

# This for loop pulls out the canonical content ID number from the true_referrer URL path and writes it back to the related article dataset
canonical = []
for i in am_ipid_pvs_detail_df['merchandized_url']:
    try: 
        canonical.append(str(int(i[i.find('php') - 9:i.find('php')-1:])))
    except:
        canonical.append(np.nan)

am_ipid_pvs_detail_df['canonical_content_id'] = canonical

# map the publications to match other datasets
am_ipid_pvs_detail_df['publication'] = am_ipid_pvs_detail_df['publication'].map(site_map)
# label the data rows in this set as "Related"
am_ipid_pvs_detail_df['link_type'] = 'Related'
# reformat visited_week to datetime for future merges
am_ipid_pvs_detail_df['visited_week'] = pd.to_datetime(am_ipid_pvs_detail_df['visited_week'])
# reformat page_views as floats for future merges
am_ipid_pvs_detail_df['page_views'] = am_ipid_pvs_detail_df['page_views'].astype(float)

# Use groupby here to get rid of the merchandised url because some of them have query strings, and so we are left with just content ids
am_ipid_pvs_detail_df1 = am_ipid_pvs_detail_df.groupby(['visited_week','publication','related_content_id','canonical_content_id','link_type'],as_index=False).sum()

# pull the data from the manually updated commerce sheet tracking which articles have been merchandised by the team. 
commerce_sheet = gc.open_by_url("https://docs.google.com/spreadsheets/d/1y0IUrwWsm1D1sZV9iqj6pRTCBnICliD3Hii1XMYOLMU/edit?pli=1#gid=559836432").worksheet("Summary")
commerce_sheet_df = pd.DataFrame(commerce_sheet.get_all_values()[1:],columns=commerce_sheet.get_all_values()[0])

# map the publication names to match the other datasets for merges
commerce_sheet_df['publication'] = commerce_sheet_df['publication'].map(site_map)

# rename dataset. These are the visited articles with ipid=article-merchandiser
ipid_articles_visited = am_ipid_pvs_df

# Create label column with "Related" and reformat dates and floats
ipid_articles_visited['link_type'] = "Related"
ipid_articles_visited['visited_week'] = pd.to_datetime(ipid_articles_visited['visited_week'])
ipid_articles_visited['page_views'] = ipid_articles_visited['page_views'].astype(float)

# Group by week while removing content_ids
ipid_articles_visited_weekly = ipid_articles_visited.groupby(['visited_week','publication','link_type'],as_index=False).sum()
ipid_articles_visited_weekly.columns = ['visited_week','publication','link_type','related_pvs','related_articles_visited']

#%%
############

# we need to get a list of canonical_content_ids to use in the next SQL queries to limit searches to these specific content ids
merchandised_content_ids = commerce_sheet_df['canonical_content_id'].to_list()
# this list is just for direct articles
merchandised_content_ids_direct = commerce_sheet_df['canonical_content_id'][commerce_sheet_df['link_type'] == 'Direct']

# this is the SQL to pull the skimlinks data for the direct content_ids
skimlinks_rev_direct = """
    # https://hearst.looker.com/explore/commerce/skimlinks_comm?toggle=fil&qid=P9aVCU91zCLby0gMmYSzk4
    SELECT
        (FORMAT_TIMESTAMP('%F', TIMESTAMP_TRUNC(skimlinks_comm.click_date , WEEK(MONDAY)))) AS skimlinks_comm_click_week,
        hnp_account_mapping.UnifiedPublicationName  AS hnp_account_mapping_unified_publication_name,
        CASE WHEN
          skimlinks_comm.normalized_page_url LIKE "%chron.com/shopping/cs/%" THEN CAST(REGEXP_EXTRACT(skimlinks_comm.normalized_page_url, r'\d+$') AS STRING)
        WHEN
          skimlinks_comm.normalized_page_url like "%.php%" THEN CAST(REGEXP_EXTRACT(skimlinks_comm.normalized_page_url, r'\-(\d*).php') AS STRING)
        ELSE
          NULL
      END   AS skimlinks_comm_article_content_id,
        COALESCE(SUM(CASE WHEN cancelled IS NOT true AND items_count != 0 THEN 1 ELSE 0 END), 0) AS skimlinks_comm_sales,
        COALESCE(SUM(skimlinks_comm.publisher_commission_amount ), 0) AS skimlinks_comm_publisher_commission_amount
    FROM `newspapers-142716.skimlinks.comm`
        AS skimlinks_comm
    INNER JOIN common.hnp_account_mapping  AS hnp_account_mapping ON hnp_account_mapping.SkimlinksPublisherDomainId LIKE CONCAT('%', (CAST(skimlinks_comm.publisher_domain_id AS STRING)), '%')
    WHERE (CASE
            WHEN
            skimlinks_comm.normalized_page_url LIKE "%chron.com/shopping/cs/%" THEN CAST(REGEXP_EXTRACT(skimlinks_comm.normalized_page_url, r'\d+$') AS STRING)
            WHEN
            skimlinks_comm.normalized_page_url like "%.php%" THEN CAST(REGEXP_EXTRACT(skimlinks_comm.normalized_page_url, r'\-(\d*).php') AS STRING)
            ELSE
            NULL
        END  ) IN UNNEST(@merchandised_content_ids_direct)
        AND ((( CAST(skimlinks_comm.transaction_datetime AS TIMESTAMP) ) >= ((TIMESTAMP_ADD(TIMESTAMP_TRUNC(TIMESTAMP_TRUNC(CURRENT_TIMESTAMP(), DAY), WEEK(MONDAY)), INTERVAL (-8 * 7) DAY))) AND ( CAST(skimlinks_comm.transaction_datetime AS TIMESTAMP) ) < ((TIMESTAMP_ADD(TIMESTAMP_ADD(TIMESTAMP_TRUNC(TIMESTAMP_TRUNC(CURRENT_TIMESTAMP(), DAY), WEEK(MONDAY)), INTERVAL (-8 * 7) DAY), INTERVAL (8 * 7) DAY)))))
    GROUP BY
        1,2,3
"""

query_params = [
    bigquery.ScalarQueryParameter('Site', 'STRING', ''),
    bigquery.ArrayQueryParameter('merchandised_content_ids', 'STRING', merchandised_content_ids),
    bigquery.ArrayQueryParameter('merchandised_content_ids_direct', 'STRING', merchandised_content_ids_direct),
    ]

job_config = bigquery.QueryJobConfig()
job_config.query_parameters = query_params
skimlinks_rev_df = client.query(
    skimlinks_rev_direct, job_config=job_config).to_dataframe()

skimlinks_rev_df['skimlinks_comm_click_week'] = pd.to_datetime(skimlinks_rev_df['skimlinks_comm_click_week'])
skimlinks_rev_df.columns = ['click_week','publication','canonical_content_id','sales','commission']

skimlinks_rev_df['visited_week'] = skimlinks_rev_df['click_week'] - np.timedelta64(1,'D')

skimlinks_rev_df['publication'] = skimlinks_rev_df['publication'].map(site_map)

# Set up a skimlinks dataframe grouped by week
skimlinks_rev_df2 = skimlinks_rev_df[['publication','visited_week','sales','commission']].groupby(['publication','visited_week'],as_index=False).sum()

skimlinks_rev_df2['link_type'] = 'Direct'
# This is the SQL to run to only get the refined list of content_ids
content_id_sql = """
    # https://hearst.looker.com/explore/newspapers/ga_web_sessions?toggle=fil&qid=lBIjhQ0NsmxCl8sPZUjb52
    
    WITH ga_web_sessions AS (SELECT _PARTITIONTIME AS partitiontime, *, (SELECT MAX(PagePath) FROM UNNEST(Hits) WHERE IsEntrance) AS entrance_page_url, (SELECT ANY_VALUE(ContentType) FROM UNNEST(Hits)) AS entrance_content_type
    FROM ga.ga_web_sessions )
    ,  hnp_web_account_mapping AS (SELECT * FROM common.hnp_account_mapping WHERE AccountType = "Website"
        )
    SELECT
        (DATE(ga_web_sessions.partitiontime)) AS ga_web_sessions_visited_date_date,
        hnp_web_account_mapping.UnifiedPublicationName  AS hnp_web_account_mapping_unified_publication_name,
        ga_web_sessions_hits.ContentId  AS ga_web_sessions_hits_content_id,
        COUNT(CASE WHEN (ga_web_sessions_hits.Type = 'PAGE') THEN CONCAT(( CONCAT(CAST(ga_web_sessions.ViewId AS STRING), "-", ga_web_sessions.FullVisitorId, '|', COALESCE(CAST(ga_web_sessions.VisitNumber AS STRING),''), "-", CAST(ga_web_sessions.VisitStartTime AS STRING))  ),'|',CAST( ga_web_sessions_hits.HitNumber   AS STRING))  ELSE NULL END) AS ga_web_sessions_hits_page_views,
        COUNT(DISTINCT ga_web_sessions_hits.ContentId ) AS ga_web_sessions_hits_content_id_count
    FROM ga_web_sessions
    INNER JOIN hnp_web_account_mapping ON ga_web_sessions.ViewId = hnp_web_account_mapping.GA_ViewId

    CROSS JOIN UNNEST(ga_web_sessions.Hits) as ga_web_sessions_hits
    WHERE ((( ga_web_sessions.partitiontime ) >= ((TIMESTAMP_ADD(TIMESTAMP_TRUNC(TIMESTAMP_TRUNC(CURRENT_TIMESTAMP(), DAY), WEEK(SUNDAY)), INTERVAL (-8 * 7) DAY))) 
    AND ( ga_web_sessions.partitiontime ) < ((TIMESTAMP_ADD(TIMESTAMP_ADD(TIMESTAMP_TRUNC(TIMESTAMP_TRUNC(CURRENT_TIMESTAMP(), DAY), WEEK(SUNDAY)), INTERVAL (-8 * 7) DAY), INTERVAL (8 * 7) DAY))))) 
    AND (ga_web_sessions_hits.Type = 'PAGE'
        AND ((lower(ga_web_sessions.VisitReferrerDomain)) NOT LIKE '%olive%' AND LOWER((lower(ga_web_sessions.VisitReferrerDomain))) NOT LIKE '%eedition%' OR (lower(ga_web_sessions.VisitReferrerDomain)) IS NULL)
        AND (ga_web_sessions.UtmCampaign NOT LIKE 'arb_%' OR ga_web_sessions.UtmCampaign IS NULL)
        AND (ga_web_sessions.entrance_page_url NOT LIKE '%bot-traffic.icu%')) 
    AND ((UPPER(( hnp_web_account_mapping.UnifiedPublicationName  )) = UPPER('SFGate') 
        OR UPPER(( hnp_web_account_mapping.UnifiedPublicationName  )) = UPPER('MySA.com') 
        OR UPPER(( hnp_web_account_mapping.UnifiedPublicationName  )) = UPPER('Chron.com') 
        OR UPPER(( hnp_web_account_mapping.UnifiedPublicationName  )) = UPPER('SeattlePI'))) 
    AND ((ga_web_sessions_hits.ContentId ) IN UNNEST(@merchandised_content_ids))
    GROUP BY
        1,
        2,3
"""

job_config = bigquery.QueryJobConfig()
job_config.query_parameters = query_params
content_id_visits = client.query(
    content_id_sql, job_config=job_config).to_dataframe()

# rename the columns for unification with other dataframes and update the publication mapping
content_id_visits.columns = ['visited_date','publication','canonical_content_id','page_views','articles']
content_id_visits['publication'] = content_id_visits['publication'].map(site_map)

# Merge the commerce sheet spreadsheet data to the traffic data based on canonical_ids
visited_articles = content_id_visits.merge(commerce_sheet_df[['publication','date','canonical_content_id']],how='left',on=['publication','canonical_content_id'])

# reformation date columsn to datetime
visited_articles['visited_date'] = pd.to_datetime(visited_articles['visited_date'])
visited_articles['date'] = pd.to_datetime(visited_articles['date'])

# We need to remove the visits that occured before the aricle was merchandized
visited_articles1 = visited_articles.dropna()[visited_articles['visited_date'] >= visited_articles['date']]

# we add a visited week column for groupby 
visited_articles1['visited_week'] = visited_articles['visited_date'] - (visited_articles['visited_date'].dt.weekday + 1) * np.timedelta64(1, 'D') 

# reformat the date columns of the commerce sheet and add a week columns to line up with visited_week for merges.
commerce_sheet_df['date'] = pd.to_datetime(commerce_sheet_df['date'])
commerce_sheet_df['week'] = commerce_sheet_df['date'] - (commerce_sheet_df['date'].dt.weekday + 1) * np.timedelta64(1, 'D') 

#######

# here we start a new sequence merging the visited article information back to the commerce sheet 
merchandised_articles = commerce_sheet_df.merge(visited_articles1.drop('date',axis=1),how='left',on=['publication','canonical_content_id'])

# We need to groupby to sum everything up by visited_week
merchandised_articles1 = merchandised_articles[['publication','canonical_content_id','article_title','merchandized_url','visited_week','link_type','related_content_id','destination_url','week','page_views','articles']].groupby(['publication','canonical_content_id','article_title','merchandized_url','visited_week','link_type','related_content_id','destination_url','week'],as_index=False).sum()

# Add an article count column
merchandised_articles1['articles'] = 1

# Merge the article data with the skimlinks data to add columsn for sales and commission
merchandised_articles2 = merchandised_articles1.merge(skimlinks_rev_df,how='left',on=['visited_week','publication','canonical_content_id'])

# Group by publication, visited_week and link_type to get summation
merchandised_articles3 = merchandised_articles2[['publication','visited_week','link_type','page_views','articles']].groupby(['publication','visited_week','link_type'],as_index=False).sum()

# merge the related article pv and article count rows, creating a second row for each week, 1 for related and 1 for direct with NaN values in direct linktype rows
merchandised_articles4 = merchandised_articles3.merge(ipid_articles_visited_weekly,how='left',on=['publication','visited_week','link_type'])

# merge in the commerce data for both direct and related skimlinks
# related commerce data is duplicated on every row as there is no way to track commerce data from related articles
merchandised_articles5 = merchandised_articles4.merge(pd.concat([am_ipid_commerce_df.drop('related_content_id',axis=1).groupby(['click_week','publication','ipid','visited_week','link_type'],as_index=False).sum(),skimlinks_rev_df2]),how='left',on=['visited_week','publication','link_type'])


# New Branch to get the article detailed data 
merchandised_articles_list = merchandised_articles2[['publication','visited_week','link_type','page_views','canonical_content_id','article_title','merchandized_url','destination_url','related_content_id','articles']].groupby(['publication','visited_week','link_type','article_title','merchandized_url','destination_url','related_content_id','canonical_content_id'],as_index=False).sum()

# bring in visits to the ipid article-merchandiser pages to show pageviews to related linked articles as well as pageviews to the original merchandised article
merchandised_articles_list2 = merchandised_articles_list.merge(am_ipid_pvs_detail_df1,how='left',on=['publication','visited_week','link_type','related_content_id','canonical_content_id'])

# create skimlinks dataframe grouped by canonical_content_id and week for direct articles
skimlinks_rev_list = skimlinks_rev_df[['publication','visited_week','sales','commission','canonical_content_id']].groupby(['publication','visited_week','canonical_content_id'],as_index=False).sum()
skimlinks_rev_list['link_type'] = 'Direct'

# merge detailed article dataframe with Related commerce data
merchandised_articles_list3a = merchandised_articles_list2[merchandised_articles_list2['link_type'] == 'Related'].merge(am_ipid_commerce_df,how='left',on=['visited_week','publication','link_type','related_content_id'])

# merge detailed article dataframe with Direct commerce data
merchandised_articles_list3b = merchandised_articles_list2[merchandised_articles_list2['link_type'] == 'Direct'].merge(skimlinks_rev_list,how='left',on=['visited_week','publication','link_type','canonical_content_id'])

# concat Related and Direct dataframes back together for the final detailed list. 
merchandised_articles_list4 = pd.concat([merchandised_articles_list3a,merchandised_articles_list3b])


def iter_pd(df):
    for val in df.columns:
        yield val
    for row in df.to_numpy():
        for val in row:
            if pd.isna(val):
                yield ""
            else:
                yield val

def pandas_to_sheets(pandas_df, sheet, clear = True):

    # Updates all values in a workbook to match a pandas dataframe
    if clear:
        sheet.clear()
    (row, col) = pandas_df.shape
    cells = sheet.range("A1:{}".format(gspread.utils.rowcol_to_a1(row + 1, col)))
    for cell, val in zip(cells, iter_pd(pandas_df)):
        if type(val) == date:
            if len(str(val)) == 7:
                cell.value = val.strftime(format='%Y-%m')
            else:
                cell.value = val.strftime(format='%Y-%m-%d')
        elif isinstance(val, datetime):
            if len(str(val)) == 7:
                cell.value = val.strftime(format='%Y-%m')
            else:
                cell.value = val.strftime(format='%Y-%m-%d')
        else:
            cell.value = val
    sheet.update_cells(cells)

article_merchandiser_data = gc.open_by_url("https://docs.google.com/spreadsheets/d/1CBgxsjLOVa39mdNhcuzru-qedyVSD9LbziaN6-P0g_Y")


pandas_to_sheets(commerce_sheet_df,article_merchandiser_data.worksheet('commerce_sheet_df'))
pandas_to_sheets(merchandised_articles5,article_merchandiser_data.worksheet('merchandised_articles'))
pandas_to_sheets(merchandised_articles_list4,article_merchandiser_data.worksheet('merchandised_articles_list'))



# %%
